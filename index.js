function countLetter(letter, sentence) {
    let result = 0

    // Check first whether the letter is a single character.
    if (typeof letter == 'string' && letter.length == 1) {

    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
      for (let i = 0;i < sentence.length; i++) {
        if (letter == sentence[i] )
          result++  
      }

    } 

    // If letter is invalid, return undefined.
    else result = undefined
    return result
    
}


function isIsogram(text) {

    let countDraculaFromSesameStreet = 0
    // The function should disregard text casing before doing anything else.
    text = text.toLowerCase();
    
    // If the function finds a repeating letter, return false. Otherwise, return true.
    for (let i = 0; i < text.length; i++) {
      for (let j = i + 1; j < text.length; j++) {
        if (text[i] === text [j]) countDraculaFromSesameStreet++
      }
    }
    if (countDraculaFromSesameStreet  > 0) return false
    return true
}


function purchase(age, price) {

    price = Math.round(price)
    let discount = price * 0.8
    let result 

    // Return undefined for people aged below 13
    if (age < 13) return undefined

    // Return the rounded off price for people aged 22 to 64.
    if (age > 21 && age < 64 ) {
      result = price
      return result
    }

    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    result = discount
    return result

    // The returned value should be a string.
} 
    


function findHotCategories(items) {
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.
    //return ['toiletries', 'gadgets'] :)

    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.
    let result = []

    for (let i of items) {
      if (i.stocks == 0 && !result.includes(i.category)) {
        result.push(i.category)
      } 
    }
    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    return result
}

function findFlyingVoters(candidateA, candidateB) {
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    // return ['LIWf1l', 'V2hjZH'] :))
    let result = []
    // Find voters who voted for both candidate A and candidate B
    for (let voters of candidateA) {
      if candidateB.includes(voters)){
        result.push(voters)
      }
    }
    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH']
    return result
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};